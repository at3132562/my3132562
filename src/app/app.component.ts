import { Component } from '@angular/core';
import { Students } from './interfaces/students';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'myNgProject';
  student: Students[];
  studentId: string;
  name: string;
  lastName: string;

  ngOnInit(): void {
    this.student = [];
  }
  onAddStudent(): void {
    if (this.studentId && this.name && this.lastName) {
      this.student.push({
        studentId: this.studentId,
        name: this.name,
        lastName: this.lastName
      });
      this.studentId = undefined;
      this.name = undefined;
      this.lastName = undefined;
    }
  }

  // onDeleteStudent(index): void {
  //   // if (this.studentId && this.name && this.lastName){
  //   // this.student.({
  //   //   studentId: this.studentId,
  //   //   name: this.name,
  //   //   lastName: this.lastName
  //   // });
  //   this.student.splice(index, 1);

  //   //   this.studentId = undefined;
  //   //   this.name = undefined;
  //   //   this.lastName = undefined;
  //   // }
  // }

  onIndexDeleteStudent(event): void {
    this.student.splice(event,1);
  }

  // onIndexDeleteStudent(i: number): void {
  //   this.student.splice(i,1);
  // }
}


