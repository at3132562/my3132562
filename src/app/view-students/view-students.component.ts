import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { Students } from '../interfaces/students';

@Component({
  selector: 'app-view-students',
  templateUrl: './view-students.component.html',
  styleUrls: ['./view-students.component.css']
})
export class ViewStudentsComponent  {
  studentList: Students[];

  @Input()
  //name: string;
  set students(student: Students[]){
    this.studentList = student
  }

  @Output()
  indexDeleteStudent: EventEmitter<number> = new EventEmitter();

  onDeleteStudent(i: number): void{
     this.indexDeleteStudent.emit(i);
  }
 

}
